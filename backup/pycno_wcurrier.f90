!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCcc
!    PROGRAM NAME:  pynco_wcurrier
!   Written by Barbara Golf, SDSU with one module written by Joe Hellmers and one function written by Whitney Currier, SDSU.
!	6 Feb 2008 - 
!	This code is based on code originally derived by the SAO PAULO / JINA teams.
!
!	Problem:  Pycnoncuclear reactions for multi-component normal matter lattices.
!	This is an attempt to calculate a multicomponent high-density fusion reaction, a step-by-step recreation of
!	work previously accomplished by Gasques, et al, in "Nuclear fusion in dense matter: Reaction 
!	rate and Carbon burning."
 
!	Key case features:  this code can handle both OCP and MCP cases
!	HIGH DENSITY
!	SAO PAULO PARAMETER FREE MODEL
!	typical density ~ 1.3 * 10^9 g/cc
!
!	Two parts to the problem:
!	A) Nuclear physics:  S-factor determination, based on SAO PAULO MODEL:  references 17, 18, 19, 20, & 21
!	B) Plasma physics:  Coulomb barrier penetration in hi-p, many body system, based on Salpeter, VanHorn, and Notre Dame MCP paper
!
!	User input:  in file "Pycno_input.dat"
!	Output:
!		all data in file "all_data.dat"
!		reaction rate versus density graph info in "Rxn_rates.dat"
!		density versus distance from nuclear center info in "pf_density1.dat"
!		nuclear potential in "Vnucarray.dat"
!		Coulomb potential in "Vcoulary.dat"
!		potential energy minus lattice and vibrational energy in "VEcheck.dat"
! 	
!	
!	Mod 01: Joe Hellmers	5/2/2010
!	Read in fraction from Maximum folding potential to set to zero
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      program pycno_wcurrier
      implicit none
	  
      real, dimension(2) :: tarray		! variables for intrinsic G95 FORTRAN time delay function - evaluates how long the program takes to run
      real :: result	  				!TARRAY(1): 	User time in seconds.
										!TARRAY(2): 	System time in seconds.
										!RESULT: 	Run time since start in seconds. 
      real(8) R							!Distance between nuclei in fermi (10**-15 m is implicit - does not need to be formally written out)
      real(8) A1, A2					!number of nucleons in the first and second nuclei
      real(8) SQM_A2					!baryon number for second nuclei if SQM involved
      real(8) SQM_mass					!estimated mass of strange quark in MeV
      real(8) Z1, Z2  					! proton count for nuclei 1 and 2, respectively, nuclei 1 = target ; nuclei 2 = projectile
      real(8) CFL						! digital variable for CFL - the CFL value is 1, then the SQM is in a CFL state; if 0 - then it's not
      integer L							!orbital angular momentum (variable currently not used)
      real(8) radius, radius1, radius2 	!name of fxn, radius of the first and second nuclei in fermi
      real(8) number_density			! fxn for calculating number density, calculated from baryon number of each sphere
      real(8) ndensity1, ndensity2		! number density for each sphere, calculated from baryon number of each sphere, output of number_density fxn
      real(8) rho0_2pf					! fxn for calculating central nuclear density for normal nuclear matter ions
      real(8) rho0_A1, rho0_A2			! rho0_A1 and rho0_A2 are central densities for each normal matter ion, used for the nucleon and matter density analytical equation in the double folding potential
      integer partition					!number of subdivisions to use for the double-folding calculation
      real(8) Vcoulomb					!function that calculates the coulomb potential energy
      real(8) V_eff						!Effective potential, sum of folding potential, coulomb potential (also, L, but not used now)
      real(8) red_mass					!function that calculates the reduced mass
      real(8) mu						!reduced mass value holder
      real(8) Energy					!function that calculates the Energy of incoming (or just second (projectile) nucleon)
      real(8) E0						!energy value holder for zero-pt vibrational energy of second nucleon (or just energy of incoming particle)
      real(4) Rstep						!step size across the lattice between two fixed (target particles), input: Rstep
      integer Rmax						! size of Rarray for turning point funx	  
      real(8) rho_crust, rho_lo, rho	!max density of crust  IN G/CM**3, lowest density, current density of calculation in g/cc
      integer rho_step					!number of steps in density graph (granularity of graph)
      real(8) rho_range					! range of densities to be calculated	  
      real(8) lattice					!function that uses rho input to calculate lattice spacing (2*R)
      real(8) :: pi = 3.14159			! pi
      integer i,j						! ad hoc counters
      integer turn1,turn2				! turning points for S factor integration - units of fermi - distance measured from vibrating nucleus
      real(8) WKB						! value of WKB calculation to get through total barrier (not just coulomb barrier) between incoming and target nuclei
      real(8) ln_WKB					! natural log of the value of the WKB calculation to get through the total barrier
      real(8) Trans_total				! transmission probability through total barrier between incoming and target nuclei
      real(8) ln_Trans_total			! natural log of the total transmission probability through the barrier
      real(8) ln_sigma 					! natural log of cross section of interaction between incoming and target nuclei
      real(8) mn_mass, mn_chrg			! mean atomic mass, mean atomic charge of the nuclei in the lattice, used to calculate Salpeter's lambda
      real(8) ln_lambda					! natural log of Salpeter and Vanhorn's dimensionless length factor
      real(8) lambda					! dimensionless length factor of Salpter and VanHorn
      real(8) ln_rstar					! natural log of Salpeter and VanHorn Bohr radius term
      real(8) ln_Estar					! natural log of energy function from Salpeter and Vanhorn
      real(8) ln_Ebcc					! natural log of the Salpeter bcc lattice energy - electrostatic interaction energy just from sitting in the lattice amidst other ions
      real(8) ln_Evib					! natural log of vibrational energy (salpeter)
      real(8) S							! astrophysical S-factor - calculated using equation from PHYS REV C69 - HOW IN THE HELL DOES THIS EQUATION WORK?
      real(8) ln_S						! natural log of astrophysical S-factor
      real(8) ln_P0						! natural log of final reaction rate
      real(8) log_P0					! base 10 log of final reaction rate
      real(8) pF_density				! fxn for calculating 2pf density for each sphere as a function of distance from ion center
      real(8) this_r1, this_r2			! loop counters for the radius of each sphere
      real(8) delta_r1, delta_r2		! lstep size for the radius of each sphere
	  !.......... file control.................TAKEN FROM DR. CAL JOHNSON'S CODE (SDSU)....  pretty much verbatim
      character filename*15
      integer ilast	  
      real(8) v_fold_min

      print*,''      
      print*,'Pycnonuclear rxn calculator'
      print*,'Written by Barbara Golf and Joe Hellmers.'
      print*,'Integration code from Dr. Cal Johnson.'
      print*,''
      print*,' Calling dtime'      
call dtime(tarray, result)
!SPAGHETTI CODE FOR INPUT FILE!!!!
      print*,' Opening pycno_input.dat'      
      open(unit=1,file='pycno_input.dat',status='old',err=100)
      print*,' OK Opening pycno_input.dat'      
      goto 103
  100 continue
      print*,''
      print*,'  Batch input file does not exist.'
  101 continue
      print*,'  Enter full name of file with sample inputs:'
      print*,''
      read*,filename
      ilast=index(filename,' ') -1
      open(unit=1,file=filename(1:ilast),status='old',err=102)
      goto 103   
  102 continue
      print*,'  That file does not exist '
      goto 101
  103 continue    ! end of Dr. Johnson's spaghetti code...  no, I still don't know how the "ilast" part works....  but, "if it's not broke..."
!	Read in variables from input file
      print*,'Reading A1'
      read(1,*)A1
      print*,'Reading Z1'
      read(1,*)Z1
      print*,'Reading A2'
      read(1,*)A2
      print*,'Reading Z2'
      read(1,*)Z2
      print*,'Reading partition'
      read(1,*)partition
      print*,'Reading L'
      read(1,*)L					! not using angular momentum yet - set to 0 in input file
      print*,'Reading Rstep'
      read(1,*)Rstep
      print*,'Reading rho_crust'
      read(1,*)rho_crust			! ADJUST THIS INPUT SO THAT YOU VARY THE CEILING DENSITY - loop downward until you get to low density	  
      print*,'Reading rho_lo'
      read(1,*)rho_lo				! added this to create rxn rate versus rho GRAPHS
      print*,'Reading rho_step'
      read(1,*)rho_step				! max number of iterations / subdivisions to use for rho calculations
      print*,'Reading sqm_a2'
      read(1,*)SQM_A2				! baryon number for A2 if SQM is involved
      print*,'Reading sqm_mass'
      read(1,*)SQM_mass				! mass of strange quark in MeV
      print*,sQM_mass
      print*,'Reading CFL'
      read(1,*)CFL					! if the CFL value is 1, then the SQM is in a CFL state, if 0 - then it's not
      print*,'Reading v_fold_min'
      read(1,*)v_fold_min					! The fraction of the v_fold maximum to consider zero
      print*,'Done Reading variable'
      close(1)


!	SQM input check
      if (SQM_A2.ne.0) then
        A2 = SQM_A2
        print*,'Calculating SQM pycnonuclear reaction...'	
        if (CFL.eq.1) then
          Z2 = 0.3*(SQM_mass/150.0)*((A2)**(2.0/3.0))		! charge of CFL state for strangelet...
        else if ((CFL.eq.0).and.(A2.le.1000)) then
          Z2 = 0.1*(A2)*((SQM_mass/150.0)**(2.0))           ! charge of non-CFL stranglet state, low-baryon number
        else
          Z2 = 8.0*((SQM_mass/150.0)**(2.0))*((A2)**(1.0/3.0))     ! charge of non-CFL stranglet state, high-baryon number		
        end if
      end if

!	open OUTPUT files
      open(unit=13,file='pF_density1.dat',status='unknown')
      open(unit=15,file='Rxn_rates.dat',status='unknown')
      open(unit=16,file='all_data.dat',status='unknown')
	  
!	Calculate the radii and central density of your two interacting nuclei... 
      radius1 = radius(A1)
      rho0_A1 = rho0_2pF(A1,radius1)
      write(16,*)'rho_0_2pF =',rho0_A1,'for ion w/ A =',A1	
      if (SQM_A2.eq.0) then
        radius2 = radius(A2)
        rho0_A2 = rho0_2pF(A2,radius2)
        write(16,*)'rho_0_2pF =',rho0_A2,'for ion w/ A =',A2	  
      else                                                       
        radius2 = A2**(1.0/3.0)					! p.5, Physical Props of Strangelets [Madsen]; first approximation... 
        rho0_A2 = number_density(A2,radius2) 	! the number density of SQM nuclei in #/fm^3, density for strangelets in MIT bag model approximately constant
      end if
	    
!	Calculate density of nucleons within both nuclei	  (VERY BASIC CALCULATION - THIS VARIABLE NOT CURRENTLY BEING USED)  --this simple function is being used for SQM 
      ndensity1 = number_density(A1,radius1) ! the number density of nucleus 1 in #/fm^3
      ndensity2 = number_density(A2,radius2) ! the number density of nucleus 2 in #/fm^3
	  
!	Calculate reduced mass of "incoming" (ground state vibrating) particle coming toward lattice-bound target particle
      mu = red_mass(A1,Z1,A2,Z2)

!	Writing out inputs in 'all_data.dat' file, so I don't have to keep referencing the input file all the time...	  
      write(16,*)A1,'= A1'	
      write(16,*)Z1,'= Z1'	  
!	SQM input reminder
      if (SQM_A2.ne.0) then
        write(16,*)'Reminder that this is an SQM pycnonuclear reaction.'
        write(16,*)'   A2, Z2, etc. are SQM numbers...'		
      end if
      write(16,*)A2,'= A2'	  
      write(16,*)Z2,'= Z2'	  
      write(16,*)partition,'= partition size 4 nonlocal nuclear interaxn'	  
      write(16,*)radius1,'= radius1 in fm'
      write(16,*)radius2,'= radius2 in fm'
      write(16,*)ndensity1,'=number density nucleus1 #/fm^3, not in use'
      write(16,*)ndensity2,'=number density #/fm^3, for SQM only'
      write(16,*)L,'= L'
      write(16,*)mu,'= reduced mass in MeV/c2'
      write(16,*)''
      write(16,*)''
	  
!	Preparing density parameters for density loop  
      rho_range = log10(rho_crust)-log10(rho_lo)
      print*,'rho_range =',rho_range
      print*,'rho_step =',rho_step	
	  
!	Start rho loop to generate rxn rate per density, step rho down to bottom density	  
      do j = 0,rho_step
        if (rho_step.eq.0) then
          rho = log10(rho_crust)
          go to 104     !skip dividing by 0 below, not using a range of densities and only evaluating at one density
        end if		
        rho = log10(rho_crust) - j*(rho_range/float(rho_step))
  104   rho = 10**(rho)  
        print*,'rho =',rho
        print*,'Rstep =',Rstep

!	function based on input density to calculate "Lattice" spacing (bcc lattice, by the way...)
!		geometrically, this is the distance between body in center of cubic lattice and exterior cubic lattice points = sqrt(3)*exterior cubic lattice spacing /2  
        R = sqrt(3.0)*0.5*lattice(rho,A1,Z1)
        print*,'R (distance between lattice points) =',R  
			  
!      	find turning points first for WKB approx/integration ------ this Rmax term defines the arrays used in subroutines - if you don't define it out here, you can't scale the array sizes
        Rmax = (R / Rstep) +1.0 		!add 1.0 in case of a remainder   - RMAX defines the length of the arrays
        print*,'Rmax =',Rmax
		
!	start density function output
        delta_r1=radius1/partition			!step size for radius1
        do i=0,partition
          this_r1 = i*delta_r1
          write(13,*)this_r1,'=dist from nuclei center',pF_density(rho0_A1,radius1,this_r1),'density'
        end do		
!	end density function output

!	define the Salpeter parameters!!!!  lambda, rstar, and Estar are the units used for plasma, length, and energy
        mn_mass=(2*A1*A2)/(A1+A2)
        mn_chrg=(Z1*A2+Z2*A1)/(A1+A2)
        print*,'mn_mass =',mn_mass
        print*,'mn_charge =',mn_chrg
        ln_lambda=-log(Z1**(1./3.)+Z2**(1./3.))+log((A1+A2)/(A1*A2*Z1*Z2))+.33*log(rho)+.33*log(mn_chrg)-.33*log(mn_mass)-8.5455
        print*,'ln_lambda =',ln_lambda
        lambda = exp(ln_lambda)
        print*,'lambda =',lambda					! dimensionless inverse-length parameter, used in plasma calculations
        ln_rstar=log(A1+A2)-log(A1*A2*Z1*Z2)-31.87
        print*,'ln_rstar =',ln_rstar				! length parameter, based on Bohr radius
        ln_Estar=log(Z1*Z2)-ln_rstar-34.174			! energy parameter, called Rydberg energy, based on ground state energy from Bohr model of the hydrogen atom
        print*,'ln_Estar =',ln_Estar
        print*,'Estar =',exp(ln_Estar)
		
!	Calculate E of "incoming" (ground state vibrating) particle coming toward lattice-bound target particle
        E0=Energy(Z1,Z2,ln_rstar,ln_Estar,ln_lambda)		
        print*,'E0 =',E0		

!	Calculate Veffective and WKB integration INSIDE turn_pts function - you should have all other input parameters at this point
!		AND you CAN'T carry the V_arrays back into the main program because Rmax is a DERIVED paramater - and used as the array dimension
        ln_sigma = 0.0
        do i = 0,L
          call turn_pt(R,Rstep,Rmax,E0,mu,A1,A2,SQM_A2,Z1,Z2,radius1,radius2,rho0_A1,rho0_A2,L,partition,turn1,turn2,WKB)
          ln_Trans_total = -WKB
!          print*,''
!          print*,'ln of Total transmission Probability: ',ln_Trans_total
          ln_sigma=ln_sigma+log(612.459)-log(mu*E0)+log(2*float(i)+1)+ln_Trans_total
!          print*,'natural log of cross section: ',ln_sigma
        end do
        
	  
!  	after calculating the cross section (or natural log because of big numbers) above, move on to Astrophysical S-factor	  
        ln_S=ln_sigma+log(E0)+(Z1)*(Z2)*.0324*sqrt(mu/E0)
        print*,'ln_S =',ln_S
        print*,'log10_S =',(ln_S)*.4343

!	REACTION RATE SECTION
        ln_P0=-2.638/(sqrt(lambda))+log(rho)+log(A1*A2)-log(A1+A2)+2*log(Z1*Z2)+ln_S+1.75*ln_lambda+109.36
		
!        if ((A1.eq.A2).and.(Z1.eq.Z2)) then	--intron from MCP.pdf equations (not sure if I should use this)
!          ln_P0=-2.638/(sqrt(lambda))+log(rho)+log(A1*A2)-log(A1+A2)+2*log(Z1*Z2)+ln_S+1.75*ln_lambda+108.667
!        end if   
        print*,'ln_P0 =',ln_P0
        log_P0 = (ln_P0)*.4343
        print*,'log_P0 =',log_P0
        !print*,'lambda=',lambda
        !print*,'ln_sigma=',ln_sigma
        !print*,'float(i)=',float(i)
        !print*,'ln_Trans_total=',ln_Trans_total
				
        write(15,*)rho,' ! rho in g/cc ! ',log_P0,' ! log10_PO in 1/(cc*s)'

        write(16,*)rho,'= rho in g/cc'
        write(16,*)R,'= distance between target and incoming nuclei in fm'
        write(16,*)E0,'= E of vibrating (projectile - a.k.a. 2nd) in MeV'
        write(16,*)Rstep,'= Step size in R in fm'
        write(16,*)Rmax,'= size of R array for turning pt fxn'
        write(16,*)turn1*Rstep,'= turn1 in fm from fixed target particle'
        write(16,*)turn2*Rstep,'= turn2 in fm, start trgt repulsive core'
        write(16,*)'IF turn1 OR 2 are -#, then E starts in barrier;'
        write(16,*)'If both are -#�s, then E is always above the barrier'  
        write(16,*)ln_S,'= ln_S'
        write(16,*)ln_S*.4343,'= log_S'
        write(16,*)ln_P0,'= ln_P0'
        write(16,*)log_P0,'= log_P0'		
        write(16,*)''
        if (rho_step.eq.0) then
          print*,'Only evaluating one density, so ...'  !SKIP TO THE END at 105, just a couple lines down!
          go to 105	
        end if		  
      end do
	  
 105  print*,'All done. Results are in "all_data.dat"'
      print*,'and "Rxn_rates.dat"'
      call dtime(tarray, result)
      print *,'Total run time since start in s = ',result
      print *,'User time in s = ',tarray(1)
      print *,'System time in s = ',tarray(2)
      write(16,*)''
      write(16,*)result,'= Total run time since start in s'
      write(16,*)tarray(1),'= Total user time in s '
      write(16,*)tarray(2),'= Total system time in s '
      end  
! ------------------------------------------------------------
      real(8) function radius(A)
!	function for calculating the radii of your two nucleons... needed for 2pFermi model
      implicit none
      real(8) A
      radius = 1.31*A**(1.0/3.0)-0.84		  
      end function
! ------------------------------------------------------------
      real(8) function rho0_2pF(A,radius)
!	USE FINITE NUCLEI CALCULATION FOR NORMAL NUCLEAR MATTER 
!	AND USE BAG MODEL TO CREATE SQM DENSITY IN NS CRUST!  
      implicit none
      real(8) A							!baryon number
      real(8) radius					!radius in fm
      real(8) :: diff_nucleon = 0.5		! average nucleon diffuseness parameter in fm	
      real(8) :: diff_matter = 0.56		! average matter diffuseness parameter in fm		
      real(8) :: pi = 3.14159			! pi
      real(8) rho0						! rho0, central nuclear density
      real(4) :: dr = 0.25				! step size for r, estimating .25 < dr < 1 fm
      real :: Integrand(0:100)  		! array to hold Integrand of rho0 calculation
      integer i							! integration counter, distance from ion center (when multiplied by dr), max size 10 fm
      real(8) Fermisum					! sum of integral (without rho0)
	  
! 11 NOV 2008:  IF YOU USE A ZERO-RANGE APPROACH (NOT USING A e^-4v2/c2 term, NOT using a nonlocal approach)
!		Then you need to use diff_matter...  the spread in the diffuseness compensates for the zero-range approximation...
	  
      do i = 0,100
        Integrand(i)=(4*pi*((i*dr)**2.0))/(1+exp(((i*dr)-radius)/diff_nucleon))
      end do
      call trapezoid(100,100,Integrand,dr,Fermisum)
	
      rho0_2pF = A/(Fermisum)
      end function
! ------------------------------------------------------------
      real(8) function lattice(rho,A,Z)   !output in units of fermi  
! 	function calculates the spacing between target particles - the exterior cube around a central body in the lattice of the crust
      implicit none
      real(8) A, Z					!nucleon and proton number, respectively
      real(8) rho					!density of crust in GRAMS/CM^3
      real(8) :: proton = 938.272 	!mass of proton in MeV/c^2
      real(8) :: neutron = 939.573	!mass of neutron in MeV/c^2
      real(8) V						!volume around each nucleus 

      V=log10(Z*proton+(A-Z)*neutron)-log10(rho)+12.25119	!in this line V is actually the log(V)
      V=10.0**V													!in this line, log(V) become V
      lattice = V**(1.0/3.0)  !output is in fermi 
      end function 
! ------------------------------------------------------------
      real(8) function red_mass(A1,Z1,A2,Z2)   !output in units of MeV/c^2
!	reduced mass calculation   
      implicit none
      real(8) A1, Z1, A2, Z2		!nucleon and proton number, respectively, for target (1) and projectile (2) nuclei, respectively
      real(8) :: proton = 938.272 	!mass of proton in MeV/c^2
      real(8) :: neutron = 939.573	!mass of neutron in MeV/c^2
      red_mass=((Z1*proton+(A1-Z1)*neutron)*(Z2*proton+(A2-Z2)*neutron))/((Z1+Z2)*proton+(A1+A2-Z1-Z2)*neutron)
      end function 
! ------------------------------------------------------------
      real(8) function Energy(Z1,Z2,ln_rstar,ln_Estar,ln_lambda)	! output is in units of Rydberg energy    
!	sum of the lattice energy and the vibrational energy of particle in anisotropic harmonic oscillator potential; not quite E = 0.5 * hbar * w
      implicit none
      real(8) Z1, Z2				! proton number for target (1) and projectile (2) nuclei, respectively
      real(8) ln_lambda				! natural log of Salpeter and Vanhorn's dimensionless length factor
      real(8) lambda				! dimensionless length factor of Salpter and VanHorn
      real(8) ln_rstar				! natural log of Salpeter and VanHorn Bohr radius term
      real(8) ln_Estar				! natural log of energy function from Salpeter and Vanhorn
      real(8) ln_Ebcc				! natural log of the Salpeter bcc lattice energy - electrostatic interaction energy just from sitting in the lattice amidst other ions
      real(8) ln_Evib				! natural log of vibrational energy (salpeter)

      ln_Ebcc=0.5986+ln_lambda+ln_Estar
      print*,'ln_Ebcc =',ln_Ebcc
      print*,'Ebcc =',exp(ln_Ebcc)
      ln_Evib=0.6162+0.5*ln_lambda+ln_Ebcc
      print*,'ln_Evib =',ln_Evib
      print*,'Evib =',exp(ln_Evib)	  
      Energy=exp(ln_Ebcc)+exp(ln_Evib)
      end function 
!------------------------------------------------------------------------
!	subroutine for calculating turning points for S-factor integral
!	finds the turning points by looking for the places where Veff - E changes sign.  	
      subroutine turn_pt(R,Rstep,Rmax,E0,mu,A1,A2,SQM_A2,Z1,Z2,radius1,radius2,rho0_A1,rho0_A2,L,partition, turn1,turn2,WKB)
      implicit none
      real(8) R, radius1, radius2  			!Distance between nuclei in fermi (10e-15 m); radii of the first and second nuclei in fermi
      real(8) rho0_A1, rho0_A2				! central densities for each sphere, input from main program fnx rho0_2pF, used to calculate pF_densities for double-folding nuclear potential
      integer partition						! number of subdivisions to use for the calculation
      real(8) A1, A2 						! number of nucleons in first and second nucleus;
      real(8) SQM_A2						!baryon number for second nuclei if SQM involved
      real(8) Z1, Z2  						! proton count for nuclei 1 and 2, respectively
      real(8) V_2fold						!double folding potential calculated with distance of R between incoming and target nuclei
      real(8) E0							!zero-pt vibrational energy of incoming nucleon (1st)
      real(8) ndensity1, ndensity2			! number density of nuclei 1 and 2, calculated using baryon number
      integer L								!orbital angular momentum (not in use) 
      real(4) Rstep							! lattice step size (user input)
      integer Rmax							!max size of array for R lattice
      real :: VEcheck(0:Rmax)	 	    	! array to hold difference between Veff and E of incoming particle at every point R (between incoming & target nuclei)
      real :: Vnucarray(0:Rmax)	    	   	! array to hold Veff between target & incoming particle at every point along R (between incoming & target nuclei)
      real :: Vcoulary(0:Rmax)				!array to hold Vcoulomb between traget and incoming particle at every point along R (between incoming & target nuclei)
      integer i, turn_counter				! ad hoc counter, counter for the number of turning points 
      integer turn1, turn2					! position along R-axis of first and second turning point
      real(8) R_pos							! current position along R axis --this is the CURRENT separation between target and incoming particle	  
      real(8) Vcoulomb						!function that calculates the coulomb potential energy
      real(8) mu							!reduced mass value holder 
      real(8) Energy						!function that calculates the Energy of incoming (or just second (projectile) nucleon)
      real :: Integrand(0:Rmax)  			! array to hold Integrand of S-factor calculation
      real(8) WKB							! value of WKB calculation to get through total barrier (not just coulomb barrier) between incoming and target nuclei
      real(8) ln_WKB						!natural log of the value of the WKB calculation to get through the total barrier
      real(8) S								! astrophysical S-factor - calculated using equation from PHYS REV C69 --rule of thumb model fitted to data
      real(8) ln_S							! natural log of astrophysical S-factor
      integer v_fold_threshold_flg, v_fold_first_time
      real(8) v_fold_max
      real(8) v_fold_min

  
	
      v_fold_min = 1e-10

      i = 0
      turn_counter = 0
      turn1 = -1
      turn2 = -1
      
!	Fill in VEcheck array first with values of Veff - E....  Veff is composed of Vnuc + Vcoul 
!	NOTE:  R_pos is the distance between the two nuclei centers!  You are essentially starting the incoming particle right next to the target particle and then backing up
!		to the starting separation distance of R ...   you are calculating VEcheck array IN REVERSE, starting where nuclei are touching (Rpos = 0) and then moving incoming
!		particle backward to Rpos = R...  So, for my visual sake, we are filling in VEcheck array from right to left (starting at Rmax and moving left to 0)
      v_fold_max = 0
      v_fold_threshold_flg = 0
      v_fold_first_time = 1
      do i = 0,Rmax
        R_pos = i * Rstep

        if (v_fold_threshold_flg .EQ. 0) then 
		call double_folding(R_pos,E0,mu,A1,A2,SQM_A2,Z1,Z2,radius1,radius2,rho0_A1,rho0_A2,partition,V_2fold)		
		if (v_fold_max .LT. abs(v_2fold)) then
			v_fold_max = abs(v_2fold)
		end if
		if (v_fold_first_time .EQ. 1) then
			v_fold_first_time = 0
		else
			if ((abs(v_2fold)/v_fold_max) .LT. v_fold_min) then
				v_fold_threshold_flg = 1			
				v_2fold = 0
			end if
		end if  
	else
		v_2fold = 0
	end if
        Vnucarray(Rmax-i) = V_2fold
        Vcoulary(Rmax-i) = Vcoulomb(Z1,Z2,R_pos,radius1,radius2)
        VEcheck(Rmax-i) = V_2fold + Vcoulomb(Z1,Z2,R_pos,radius1,radius2) - E0	
      end do
	  
!	now check the VEcheck array for turning point(s) - there may be more than one - especially if the total energy of the incoming particle starts out high (greater than the potential)
      do i = 1,Rmax
        if ((VEcheck(i).le.0).and.(VEcheck(i-1).ge.0)) then   		!if Veff - E(i) starts positive and goes negative....
          turn_counter = turn_counter + 1							!then you've found a turning point....	
          print*,'in first if statement, turn1=',turn1
          print*,'in first if statement, turn2=',turn2
          if (turn1.lt.0) then
            turn1 = i - 1										  	!and the first turn just prior to this position
          else 													
            turn2 = i - 1
          end if
          print*,'after first i-1 if statement, turn1=',turn1
          print*,'after first i-1 if statement, turn2=',turn2
        else if ((VEcheck(i).ge.0).and.(VEcheck(i-1).le.0)) then 	!if Veff - E(i) starts negative and goes positive...
          turn_counter = turn_counter + 1							!another turn....
          print*,'in 2nd part of VEcheck if statement, turn1=',turn1
          print*,'in 2nd part of VEcheck if statement, turn2=',turn2
          if (turn1.lt.0) then
            turn1 = i - 1										  	!and the turn is just prior to this position
          else										
            turn2 = i - 1
          end if 
          print*,'after 2nd i-1 if statement, turn1=',turn1
          print*,'after 2nd i-1 if statement, turn2=',turn2
        end if
      end do
	  
!	print arrays to output files
      open(unit=17,file='Vnucarray.dat',status='unknown')
      open(unit=18,file='VEcheck.dat',status='unknown')
      open(unit=19,file='Vcoulomb.dat',status='unknown')
      open(unit=33,file='check1.dat',status='unknown')
      open(unit=34,file='check2.dat',status='unknown')
      do i =0,Rmax
        write(17,*)Vnucarray(i),'! Varray(',i,')'
        write(18,*)VEcheck(i),'! VEcheck(',i,')'
        write(19,*)Vcoulary(i),'! Vcoulomb(',i,')'
      end do

!	Start WKB integration here... cannot go back up to main program because of Rmax as vector parameter (derived input) - not defined at start of main program 

!	Just use Trapezoid rule (very basic) for integration to determine WKB calculation
!		Equation from PHYS REV C69, 034603 (2005):   WKB = Integral from R1 to R2 [ sqrt( (8*mu)/hbar^2 *(Veff[R,E] - E)) ] dR
!			(Veff[R,E] - E) is the VEcheck array.
!			See notes for "Integrand - 24 April 2008"

! check Johnson's code for quadrature examples - found several - just use trapezoidal rule for starters - can upgrade later
      if (turn2.lt.0) then
!	THERE MAY ONLY BE ONE TURNING POINT - STILL NEED TO INTEGRATE THROUGH BARRIER
        do i = 0,Rmax
          if (i.gt.turn1) then
            Integrand(i) = 0.0
          else
            Integrand(i) = .01432*sqrt(mu*VEcheck(i))    ! KEY DIFFERENCE:  hbar in SsubL def!
          end if
          write(33,*)Integrand(i),'!Integrand(',i,')'
        end do		  	  
        call trapezoid(Rmax,turn1,Integrand,Rstep,WKB)	
      else	  
!	THERE ARE USUALLY TWO TURNING POINTS WHEN USING SALPETER AND VAN HORN... NEED TO INTEGRATE THROUGH BARRIER
        do i = 0,Rmax
          if ((i.le.turn1).or.(i.gt.turn2)) then
            Integrand(i) = 0.0
             else if ((VEcheck(i).LT.0).AND.(i.GT.Rmax-20)) then 
                Integrand(i)=0
          else
            Integrand(i) = .01432*sqrt(mu*VEcheck(i))   
          end if
          write(34,*)Integrand(i),'Integrand(',i,')'
        end do	  
        call trapezoid(Rmax,turn2,Integrand,Rstep,WKB)
      end if
      
      print*,'WKB =',WKB
!  return the WKB back to the main program - and then calculate the Astrophysical S-Factor and then pycno-nuclear reaction rate with a new function
      return
      end
!----------------------------------------------------------------------------------
!Function for folding potential - returns the folding potential in MeV
!	WRITTEN BY JOE HELLMERS, SDSU
!	What this specifically means is... start from a designated center of one nucleus (point 1 or (0,0,0) in nucleus A) and calculate the total potential felt at point from
!	from every other point in nucleus B. That part is a single folding potential.  Next, move to a new point (point 2) in nucleus A and calculate the total potential felt at that point from every other point in nucleus B...
!	And continuously sum up for point 3... point 4... etc.  That's a double folding potential.    	
      subroutine double_folding(R,E0,mu,A1,A2,SQM_A2,Z1,Z2,radius1,radius2,rho0_A1,rho0_A2,partition,V_2fold)
      implicit none
      real(8) R, radius1, radius2  			!Distance between nuclei in fermi (10e-15 m); radii of the first and second nuclei in fermi
      integer partition						! number of subdivisions to use for the calculation
      real(8) A1, A2 						! number of nucleons in first and second nucleus;
      real(8) SQM_A2						!baryon number for second nuclei if SQM involved
      real(8) number_density				! fxn for calculating number density FOR SQM ONLY, calculated from baryon number of SQM sphere
      real(8) Z1, Z2  						! proton count for nuclei 1 and 2, respectively
      real(8) mu							!reduced mass value holder
      real(8) E0							!zero-point vibrational energy for first (incoming particle)
      real(8) V_2fold						!double folding potential -- MAIN OUTPUT OF THIS SUBROUTINE!!!
      integer r1, r2   						! counters from zero to radius1 and radius2 in the radii loops
      integer theta1, theta2				!total distance when multiplied by r from top of sphere to bottom of sphere (theta1, theta2 max is pi)
      integer phi1, phi2					!total distance when multiplied by r around midsection of sphere (phi1, phi2 max is 2pi
      real(8) delta_r1, delta_r2			!step size of radius1 and radius2 in the radii loops
      real(8) delta_theta1, delta_theta2	!step size of theta starting from top of sphere and angling downwards
      real(8) delta_phi1, delta_phi2		!step size of phi starting from point belly of a sphere closest to other sphere and working around
      real(8) dV1, dV2						!part of the volume of each sphere
      real(8) d								!distance between two points in the spheres (including distance between the spheres)
      real(8) accumulator1					!accumulator for total folding potential
      real(8) :: pi = 3.14159265359
      real(8) ndensity1, ndensity2			!number density for each sphere, calculated from baryon number of each sphere
      real(8) rho0_A1, rho0_A2				! central densities for each sphere, input from main program fnx rho0_2pF, used to calculate pF_densities for double-folding nuclear potential
      real(8) pF_density					! fxn for calculating 2pf density for each sphere as a function of distance from ion center
      real(8) pF_density1, pF_density2		! 2pF density for each sphere, calculated from rho_0 of each sphere (r-dependent)	  
      real(8) this_r1, this_r2				!loop counters for the radius of each sphere
      real(8) this_theta1, this_theta2		!loop counters for the elevation angle (though we start from the top of the sphere and work down!) of each sphere
      real(8) this_phi1, this_phi2			!loop counters for the azimuthal angle of each sphere (starting from closest points at midsection of each sphere)
      real(8) dx, dy, dz					!distances (in cartesian coordinate unit vectors) between two points in spheres
      real(8) nucleonint					! function for nucleon-nucleon force		
      real(8) pi_div_2						!just so i don't have to keep typing out pi/2.0
      pi_div_2 = pi/2.0
      accumulator1 = 0.0

      delta_r1=radius1/partition			!step size for radius1
      delta_r2=radius2/partition			!step size for radius2
      delta_theta1 = pi_div_2/partition		!For dtheta1 and dphi1, we are only moving from the top of the sphere1 to its midpoint and then
      delta_phi1 = pi/partition				!going from the midpoint closest to sphere2 to the point farthest from sphere2,
      delta_theta2 = pi/partition			!calculating the folding potential for only a 1/4 of sphere1 and then multiplying by four for the total folding potential.
      delta_phi2 = 2*pi/partition			!step size for the phi angle of the second sphere
	
      do r1=1,partition
        this_r1 = r1*delta_r1-delta_r1/2.0
		
        do theta1=1,partition
           this_theta1 = theta1*delta_theta1-delta_theta1/2.0
													!the pi_div_2 in phi1 calculation is NECESSARY because coordinate system has been defined by dx = R - x1 + x2...
          do phi1=1,partition						!x1 must be positive in hemisphere nearest sphere2, x2 must be positive in hemisphere furthest from sphere1					  
             this_phi1 = phi1*delta_phi1 - delta_phi1/2.0 - pi_div_2	!this can only be done by forcing cos (phi) to be positive in those hemispheres, so integration limits are -pi/2 to pi/2!!!	
				
            do r2=1,partition
               this_r2 = r2*delta_r2-delta_r2/2.0
					
              do theta2=1,partition
                 this_theta2 = theta2*delta_theta2-delta_theta2/2.0
						
                do phi2=1, partition
                  this_phi2 = phi2*delta_phi2 - delta_phi2/2.0
							
                  dV1 = this_r1*this_r1*sin(this_theta1)*delta_theta1*delta_phi1*delta_r1
                  dV2 = (this_r2)*(this_r2)*sin(this_theta2)*delta_theta2*delta_phi2*delta_r2
                  dx = R + this_r2*sin(this_theta2)*cos(this_phi2) - this_r1*sin(this_theta1)*cos(this_phi1) 
                  dy = this_r2*sin(this_theta2)*sin(this_phi2) - this_r1*sin(this_theta1)*sin(this_phi1)
                  dz = this_r2*cos(this_theta2) - this_r1*cos(this_theta1)
                  d = sqrt(dx*dx + dy*dy + dz*dz)

                  pF_density1 = pF_density(rho0_A1,radius1,this_r1)

                  if (SQM_A2.eq.0) then
                    pF_density2 = pF_density(rho0_A2,radius2,this_r2)
                  else    
                    pF_density2 = number_density(A2,radius2) 	! the number density of SQM nuclei in #/fm^3
                  end if

                  accumulator1 = accumulator1+pF_density1*pF_density2*dV1*dV2*nucleonint(d)				    


                end do
              end do
            end do
          end do
        end do
      end do

      V_2fold = 4.0*accumulator1

      print*,'V2_fold for ',R,' done. v_2fold = ',v_2fold  
      return
      end
!-----------------------------------------------------------------------------------------------------------------------
      real(8) function pF_density(rho0,radius,r)
!	2pF (r-dependent) density calculation upgrade!  FOR NORMAL NUCLEAR MATTER, from eq. 6, NL Desc Nuc Int. pdf, the units here are fm**-3  
      implicit none
      real(8) rho0						! central density
      real(8) radius					!radius of ion in fm
      real(8) r							! distance from nuclear center
      real(8) :: diff_nucleon = 0.5		! average nucleon diffuseness parameter IN NUCLEI in fm	
      real(8) :: diff_matter = 0.56		! average matter diffuseness parameter IN NUCLEI in fm	
      real(8) :: pi = 3.14159			! pi
      real(8) cond						! condition for fermi distribution analytical equation, different density equations depending on condition 

      cond = (r - radius)/diff_nucleon	
      if (cond.le.0.0) then
        pF_density = rho0*(1-0.875*exp(cond)+0.375*exp(2*cond)) 				!from eq. 7, NL Desc Nuc Int. pdf, the units here are fm**-3
      else
        pF_density=rho0*exp(-cond)*(1-0.875*exp(-cond)+0.375*exp(-2*cond)) 		!from eq. 8, NL Desc Nuc Int. pdf, the units here are fm**-3
      end if	  
      end function
! ------------------------------------------------------------
! nuclear interaction function --UNITS note:  the input "r" is assumed to be in fermi!!
!		DOUBLE-CHECK UNITS IN FINAL VLEP
     
      real(8) function nucleonint(d)
      implicit none
!     real(8) nucleonint
      real(8) d						! distance between two points in the spheres (including distance between the spheres)
      real(8) delta_0  
      delta_0 = 0.42                                

! USING THIS (4July):  using nucleon - nucleon interaction EQUATION 46, multiple times exp (-v2) term to get velocity dependent nucleon-nucleon interaction

      if (d .GT. delta_0) then
         nucleonint =(7999.0*exp(-4.0*d)/(4*d))-(2134.0*exp(-2.5*d)/(2.5*d))

      else 
         nucleonint = (7999.0*exp(-4.0*d)/(4*d))-(2134.0*exp(-2.5*d)/(2.5*d)) - 262.0

      end if
     
      end function nucleonint
!-----------------------------------------------------------------------------------------------------------------------
! Coulomb Potential function - output in MEV
      real(8) function Vcoulomb(Z1,Z2,r,radius1,radius2)
      implicit none
      real(8) r					! distance (starts in fermi!)
      real(8) radius1, radius2	! radii of the two ions
      real(8) Z1, Z2  			! proton count for nuclei 1 and 2, respectively
      if (r.lt.(radius1+radius2)) then
        Vcoulomb=((3*(radius1+radius2)**2.0)-(r**2.0))*(Z1*Z2*1.4397)/(2*(radius1+radius2)**3.0)	! Vcoulomb is in MeV
      else 
        Vcoulomb = (Z1*Z2*1.4397)/r	! Vcoulomb is in MeV
      end if	  
      end function Vcoulomb
! -----------------------------------------------------------------------------------------------------------------------
      subroutine trapezoid(Np,N,array,h,ans)
!		DOCUMENTATION NOTE:  THIS SUBROUTINE IS - IN ITS ENTIRETY - FROM DR. CALVIN JOHNSON'S COMPUTATIONAL 
! 		COURSE - FALL 2006 AT SDSU  --- CWJ - SDSU - August 2006
!		ALL CREDIT AND ERROR FROM THIS SECTION ARE TO EITHER HIS CREDIT OR HIS FAULT (and mine, I suppose, for not recognizing it...)
!  subroutine to compute integral using trapezoidal rule
! INPUT:
!   Np: declared dimension of array
!   N : used dimension of array
!   array(0:Np): array of points of function to be integrated
!   h :  dx 
! OUTPUT:
!   ans : integral
      implicit none
!..........INPUT...............
      integer Np, N        ! dimensions
      real :: array(0:Np)     ! this is legal, since being fed from outside
      real(4) h               ! = dx
!.........OUTPUT................
      real(8) ans
!.........INTERMEDIATE..........
      integer i
      ans = 0.0            ! initialize output
      ans = 0.5*(array(0) + array(N))
      do i = 1, N-1 
        ans = ans+array(i)
      enddo
      ans = ans*h
      return
      end
!-----------------------------------------------------------------------------------------------------------------------
      real(8) function number_density(A,radius)    !CURRENTLY IN USE ONLY FOR FIRST APPROX. FOR SQM
!	THIS DENSITY CALCULATION IS too simplistic!  NEED RHO(R)!	USE FINITE NUCLEI CALCULATION FOR NORMAL NUCLEAR MATTER AND USE
!	BAG MODEL TO CREATE SQM DENSITY IN NS CRUST!  
      implicit none
      real(8) A							!baryon number
      real(8) radius					!radius in fm
      real(8) :: pi = 3.14159			! pi
      number_density = (3.0/4.0)*A/(pi*radius**3)	  
      end function
!-------------------------------------------------------------------------------------------------------------------
